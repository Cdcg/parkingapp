package com.example.parkingappv3;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.parkingappv3.DbPersistencia.DbUsuarios;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;

public class MainActivity extends AppCompatActivity {

    EditText edit_usuario,edit_contraseña;
    Button btn_ingresar,btn_registro;
    DbUsuarios DB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edit_usuario = findViewById(R.id.edit_usuario);
        edit_contraseña=findViewById(R.id.edit_contraseña);

        btn_registro=findViewById(R.id.btn_registro);
        btn_ingresar=findViewById(R.id.btn_ingresar);

//        if(BuildConfig.DEBUG){
//            edit_usuario.setText("Test");
//            edit_contraseña.setText("test123");
//        }

        DB = new DbUsuarios(this);



        // Nuevo Codigo
        btn_ingresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String user = edit_usuario.getText().toString(); // Tomar los datos que agregó el usuario
                String pass = edit_contraseña.getText().toString();

                if (TextUtils.isEmpty(user) || TextUtils.isEmpty(pass))
                    Toast.makeText(MainActivity.this, "Todos los espacios son requeridos", Toast.LENGTH_SHORT).show();
                else {
                    DB.checkcontrasena(user, pass,new OnCompleteListener<QuerySnapshot>() {
                        @Override
                        public void onComplete(@NonNull Task<QuerySnapshot> task) {
                            Toast.makeText(MainActivity.this, "Verificando usuario", Toast.LENGTH_SHORT).show();
                            if(task.isSuccessful()){ // verificamos si se hizo correctamente la peticion al server
                                for (DocumentSnapshot doc : task.getResult()){
                                    if(doc.exists()){
                                        // Si el usuario ya existe.
                                        String contraenfirebase = doc.getString("contrasena");
                                        if (contraenfirebase.equals(pass)) {
                                            Toast.makeText(MainActivity.this, "Login correcto", Toast.LENGTH_SHORT).show();


                                            /*Dialogo d = new Dialogo(MainActivity.this,"Aqui va el titulo","Hola gente", new View.OnClickListener() {
                                                @Override
                                                public void onClick(View view) {
                                                    Intent intent = new Intent(getApplicationContext(), CardsItems.class);
                                                    startActivity(intent);
                                                }
                                            });*/


                                        } else {
                                            Toast.makeText(MainActivity.this, "Login incorrecto", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                    else {
                                        //El usuario no existe.
                                        // Aquí lo insertaríamos.
                                    }
                                }
                            } else {
                                // Hubo un error en la conexión.
                            }
                        }
                    });
                }
            }
        });

        btn_registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,Formulario.class);
                startActivity(intent);
                finish();
            }
        });

        /*btn_registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Cambiamos de layout/Vista
                Intent intent = new Intent(getApplicationContext(), Formulario.class);
                startActivity(intent);
            }
        });*/
        // Fin codigo nuevo


        ////////////


        // Codigo Anterior
        /*btn_ingresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this,"Presionaste Ingresar",Toast.LENGTH_LONG).show();
                Intent intent = new Intent(MainActivity.this,Parqueaderos_productos.class);
                startActivity(intent);
                finish();
            }
        });
        btn_registro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this,Formulario.class);
                startActivity(intent);
                finish();
            }
        });*/
    }
}