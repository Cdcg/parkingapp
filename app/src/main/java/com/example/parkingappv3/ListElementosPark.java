package com.example.parkingappv3;

public class ListElementosPark {

    //esta clase lista elementos, tiene la informacion base para nuestras tarjetas
    public String color; //me permitira cambiar el color a las tarjetas
    public String parqueadero,detalle,ciudad;


    //se genera este constructor con el clic derecho
    public ListElementosPark(String color, String parqueadero, String detalle, String ciudad) {
        this.color = color;
        this.parqueadero = parqueadero;
        this.detalle = detalle;
        this.ciudad = ciudad;
    }

    //se crean los geter y seter con el click derecho generate

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getParqueadero() {
        return parqueadero;
    }

    public void setParqueadero(String parqueadero) {
        this.parqueadero = parqueadero;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getCiudad() {
        return ciudad;
    }

    public void setCiudad(String ciudad) {
        this.ciudad = ciudad;
    }
}
