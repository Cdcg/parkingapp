package com.example.parkingappv3.DbPersistencia;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import androidx.annotation.Nullable;
import com.example.parkingappv3.ListElementosPark;
import java.util.ArrayList;
import java.util.List;

public class DbProductos extends DbHelper{

    Context context; // Variable global

    // Constructor
    public DbProductos(@Nullable Context context){
        super(context);
        this.context = context;
    }

    public void agregarArticulo(String nombrep, String descripcion, String precio) {
        ContentValues cv = new ContentValues(); // Instancia del objeto ContentValues
        cv.put("nombrep", nombrep);
        cv.put("descripcion", descripcion);
        cv.put("precio", precio);
        this.getWritableDatabase().insert("productos", null, cv);
    }

    public void eliminarArticulo(String codigop) {
        this.getWritableDatabase().delete("productos", "codigop = ?", new String[]{codigop.trim()});
    }

    public void actualizarArticulo(String codigop, String descripcion, String precio) {
        ContentValues cv = new ContentValues(); // Instancia
        cv.put("descripcion", descripcion);
        cv.put("precio", precio);
        // trim = Es un método que se encarga de eliminar caracteres blancos iniciales y finales de una cadena de texto (String)
        this.getWritableDatabase().update("productos", cv, "codigop = ?", new String[]{codigop.trim()});
    }

    public List<ListElementosPark> consultarArticulos() {
        List<ListElementosPark> listArticulos = new ArrayList<ListElementosPark>(); // Instancia de un objeto tipo lista

        Cursor result = this.getWritableDatabase().query("productos", new String[]{"codigop", "nombrep", "descripcion", "precio"}, null, null, null, null, null);
        while (result.moveToNext()) {
            ListElementosPark nuevoArticulo = new ListElementosPark(
                    result.getString((int) result.getColumnIndex("color")),
                    result.getString((int) result.getColumnIndex("parqueadero")),
                    result.getString((int) result.getColumnIndex("detalle")),
                    result.getString((int) result.getColumnIndex("ciudad"))
            );
            listArticulos.add(nuevoArticulo);
        }
        return listArticulos;
    }
}