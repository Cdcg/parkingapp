package com.example.parkingappv3.DbPersistencia;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import androidx.annotation.Nullable;

public class DbHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1; //el versionado de nuestra app
    private static final String DATABASE_NOMBRE = "parking.db"; //la creacion de la base de datos
    private static final String TABLE_USERS = "usuarios";

    public DbHelper(@Nullable Context context) {
        super(context, DATABASE_NOMBRE,null,DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) { //creacion de las tablas de la data base

        sqLiteDatabase.execSQL(" CREATE TABLE " + TABLE_USERS + "(" +
                "idusuario INTEGER PRIMARY KEY AUTOINCREMENT,"+
                "edit_nombres TEXT not null,"+
                "edit_apellidos TEXT not null," +
                "edit_correo TEXT not null," +
                "edit_contraseña TEXT not null)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL(" DROP TABLE " + TABLE_USERS); //genera la consulta
        onCreate(sqLiteDatabase); //creacion de la base de datos
    }
}