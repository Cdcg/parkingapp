package com.example.parkingappv3.DbPersistencia;

import android.content.Context;

import androidx.annotation.Nullable;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.firestore.CollectionReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.Query;

import java.util.HashMap;
import java.util.Map;

public class DbUsuarios extends DbHelper{

    Context context; // Variable global
    FirebaseFirestore firebaseFirestore;
    FirebaseFirestore dbf = FirebaseFirestore.getInstance();

    // Constructor
    public DbUsuarios(@Nullable Context context) {
        // SUPER: Llama al contructor de la clase padre
        super(context);
        this.context = context;
    }

    public void insertarUsuario(String edit_nombres, String edit_apellidos, String edit_correo, String edit_contraseña, OnSuccessListener metodo, OnFailureListener metodomal) {
        Map<String,String> data = new HashMap<String,String>();
        data.put("edit_nombres",edit_nombres);
        data.put("edit_apellidos",edit_apellidos);
        data.put("edit_correo",edit_correo);
        data.put("edit_contraseña",edit_contraseña);
        dbf.collection("usuarios").document().set(data).addOnSuccessListener(metodo).addOnFailureListener(metodomal);
    }

    public void checknomusuario(String edit_nombres, OnCompleteListener metodo){ //
        // NEW
        Query q = dbf.collection("usuarios").whereEqualTo("edit_nombres", edit_nombres);
        q.get().addOnCompleteListener(metodo);
    }


    public void checkcontrasena(String edit_nombres, String edit_contraseña,OnCompleteListener metodo){

        CollectionReference usersRef = dbf.collection("edit_nombres");

        Query query = dbf.collection("usuarios").whereEqualTo("edit_nombres", edit_nombres);
        query.get().addOnCompleteListener(metodo);
    }


}