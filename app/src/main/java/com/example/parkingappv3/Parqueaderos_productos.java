package com.example.parkingappv3;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.TypedValue;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class Parqueaderos_productos extends AppCompatActivity {

   // TextView textView ;
    Button btn_atras2, btn_fragment;
    TextView tv1; // traido de menuensayo

    List<ListElementosPark> elementos; //de manera global

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parqueaderos_productos);

        // traido de menuensayo
        //setContentView(R.layout.activity_menuensayo);
        //tv1 = findViewById(R.id.tv1);

        btn_atras2= findViewById(R.id.btn_atras2);
        btn_fragment = findViewById(R.id.btn_fragment);

        //textView = findViewById(R.id.titulo_recicler);

        iniciar();

        btn_atras2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(Parqueaderos_productos.this,MainActivity.class);
                startActivity(intent);
                finish();
            }
        });

        btn_fragment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Parqueaderos_productos.this,FragmentMain.class);
                startActivity(intent);
                finish();
            }
        });

    }

    //traido de menuensayo
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menudeopciones, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int nro = item.getItemId();
        float valor;

        //SET = Modificar
        //GET = Obtener

        switch (nro){
            case R.id.agrandarfuente:
                //valor = tv1.getTextSize();
                //valor = valor + 20;
                //tv1.setTextSize(TypedValue.COMPLEX_UNIT_PX, valor);
                return true;
            case R.id.reducirfuente:
                //valor = tv1.getTextSize();
                //valor = valor - 30;
                //tv1.setTextSize(TypedValue.COMPLEX_UNIT_PX, valor);
                return true;
            case R.id.salir:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void iniciar() {

        elementos = new ArrayList<ListElementosPark>();// traigo la lista array y agrego nuevos elementos a esa lista
        elementos.add(new ListElementosPark("#607d8b", "Parqueaderos Cúcuta", "Cúcuta", "Ver máss"));
        //envio las caracteristicas nuevas para cada tarjeta
        elementos.add(new ListElementosPark("#775447", "Parqueaderos Bogotá", "Bogotá", "Ver más"));//tarjeta#2
        elementos.add(new ListElementosPark("#009688", "Parqueadero Bucaramanga", "Bucaramanga", "Ver más"));//tarjeta#3

        //declaramos un lis adapter y un context de donde viene
        ListAdapter listAdapter = new ListAdapter(elementos,this);

        RecyclerView reciclrevie = findViewById(R.id.vista_recicler_productos); //que se encuentra en el layout productos

        reciclrevie.setHasFixedSize(true);

        //todas las tarjetas de manera vertical una debajo de la otra
        reciclrevie.setLayoutManager(new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false));

        reciclrevie.setAdapter(listAdapter);//se ejecuta mediante esa linea
    }
}