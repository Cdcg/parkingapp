package com.example.parkingappv3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;

public class FragmentMain extends AppCompatActivity {

    FragmentTransaction transaction;
    Fragment fragmentInicio, fragment_uno, fragment_dos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_main);

        fragmentInicio = new InicioFragment();
        fragment_uno = new Fragment_Uno();
        fragment_dos = new Fragment_Dos();

        getSupportFragmentManager().beginTransaction().add(R.id.contenedorFragments, fragmentInicio).commit();

    }

    public void onClick(View view){
        // FragmentManager = Pueden agregar, quitar, reemplazar y realizar otras acciones con fragmentos,
        // en respuesta de la interacción del usuario con nuestra app.

        transaction = getSupportFragmentManager().beginTransaction();
        switch (view.getId())
        {
            case R.id.btnRojo: transaction.replace(R.id.contenedorFragments, fragment_uno);
                transaction.addToBackStack(null); // Cuando le demos en el botón "atras" no nos cierre la app
                break;
            case R.id.btnVerde: transaction.replace(R.id.contenedorFragments, fragment_dos);
                transaction.addToBackStack(null);
                break;
        }
        transaction.commit();
    }
}