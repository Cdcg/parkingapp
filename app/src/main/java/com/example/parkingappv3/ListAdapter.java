package com.example.parkingappv3;

import android.content.Context;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class ListAdapter extends RecyclerView.Adapter<ListAdapter.ViewHolder>{

    private List<ListElementosPark> mData;
    private LayoutInflater mInflater;
    private Context contexto;

    public ListAdapter (List<ListElementosPark> itemList,Context contexto){

    this.mInflater= LayoutInflater.from(contexto);
    this.contexto= contexto;
    this.mData =itemList;

    }

    @Override
    public int getItemCount() {

        return mData.size();
    }


    @Override
    public ListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = mInflater.inflate(R.layout.lista_elementos_tarjetas,parent,false);
        return new ListAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ListAdapter.ViewHolder holder, final int POSITION) {
        holder.binData(mData.get(POSITION));// traemos el metodo binData creado abajo
    }


    public void setItems(List<ListElementosPark>items){
    mData=items;

    }

    //se crea la sub clase que extiende del recicler
    public class ViewHolder extends RecyclerView.ViewHolder{
        //declaracion de variables

        ImageView iconimage;
        TextView nombre,detalle,ciudad;

        public ViewHolder(View itemView) {
            super(itemView);

            iconimage=  itemView.findViewById(R.id.img_park);
            nombre = itemView.findViewById(R.id.titulo_tarjeta);// se obtiene el elemento de la tarjeta
            detalle = itemView.findViewById(R.id.descripcion_tarjeta);
            ciudad = itemView.findViewById(R.id.detalle);

        }

        void binData(final ListElementosPark item){

            //porterduff describe la forma de cambiar imagenes

            iconimage.setColorFilter(Color.parseColor(item.getColor()), PorterDuff.Mode.SRC_IN);

            nombre.setText(item.getParqueadero()); //me trae el nombre y lo modifica
            detalle.setText(item.getDetalle()); //me trae el detalle y lo modifica
            ciudad.setText(item.getCiudad());
        }




    }




}
