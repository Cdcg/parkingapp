package com.example.parkingappv3;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.parkingappv3.DbPersistencia.DbHelper;
import com.google.android.material.snackbar.Snackbar;

public class Formulario extends AppCompatActivity {

    EditText edit_nombres,edit_apellidos,edit_correo,edit_contraseña;
    Button btn_registrar, btn_atras;
    Dialog dialogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario);
        //se buscan los elementos
        edit_nombres = findViewById(R.id.edit_nombres);
        edit_apellidos = findViewById(R.id.edit_apellidos);
        edit_correo = findViewById(R.id.edit_correo);
        edit_contraseña = findViewById(R.id.edit_contraseña);
        btn_registrar = findViewById(R.id.btn_registrar);
        btn_atras= findViewById(R.id.btn_atras);
        //se traera el dialogo desde la estrustura
        dialogo = new Dialog(Formulario.this);
        dialogo.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogo.setContentView(R.layout.mensaje_estructura);
        dialogo.setCanceledOnTouchOutside(false);
        //con estas lineas modifico el contenido del layout estructura del mensaje
        ImageView iimagen = (ImageView) dialogo.findViewById(R.id.imagen);
        iimagen.setImageResource(R.drawable.ic_parkin);
        TextView ttitulo1 = (TextView) dialogo.findViewById(R.id.ttitulo);
        ttitulo1.setText("¡Registro!");
        TextView tmensaje1 = (TextView) dialogo.findViewById(R.id.tmensaje);
        tmensaje1.setText("¿Quieres registrarte?.");
        //dialogo.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT);
        dialogo.setCancelable(false);
        //los botones funcionales dentro del dialog
        Button cancelar = dialogo.findViewById(R.id.btn_no);
        Button registrado= (Button)dialogo.findViewById(R.id.btn_si);

        btn_atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar snack= Snackbar.make(view,"Snack bar", Snackbar.LENGTH_SHORT).setAction("Ir atrás", new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(Formulario.this,MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                });
                /*Intent intent = new Intent(Formulario.this,MainActivity.class);
                startActivity(intent);
                finish();*/
            }
        });

        btn_registrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(Formulario.this,"Presionaste registrar, DB CREADA PAPI",Toast.LENGTH_SHORT).show();
                Snackbar snack= Snackbar.make(view,"Snack bar", Snackbar.LENGTH_SHORT);
                snack.show();

                DbHelper dbhelper = new DbHelper(Formulario.this);//estoy en la clase formulario, instancio la bd
                SQLiteDatabase db = dbhelper.getWritableDatabase();// crear la base de datos y escribir

                if (db!= null){
                    //Toast.makeText(Formulario.this,"Presionaste registrar, DB CREADA PAPI",Toast.LENGTH_SHORT).show();
                    dialogo.show();

                    cancelar.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Toast.makeText(Formulario.this,"CANCELADO",Toast.LENGTH_SHORT).show();
                            dialogo.dismiss();
                        }
                    });

                    registrado.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {

                            Toast.makeText(Formulario.this,"Usuario registrado",Toast.LENGTH_SHORT).show();
                            dialogo.dismiss();
                        }
                    });
                }else{
                    Toast.makeText(Formulario.this,"DB NO CEADA",Toast.LENGTH_SHORT).show();
                }
            }
        });

        btn_atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                startActivity(intent);
            }
        });

    }
}